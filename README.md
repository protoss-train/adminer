## Build Image
docker build . -t protosstechnology/adminer

## Push Image
docker push protosstechnology/adminer

## Run
docker run -d --name oracle -p 80:80 protosstechnology/adminer

